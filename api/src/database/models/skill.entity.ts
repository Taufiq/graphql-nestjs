import { Field, InputType, ObjectType } from "@nestjs/graphql";
import { Exclude, Expose } from "class-transformer";
import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { Content } from "./content.entity";
import { User } from "./user.entity";

@Entity('skills')
@ObjectType()
export class Skill extends Content{
  @Field(()=>String)
  @Column({ type: 'varchar', length: 100, nullable: true })
  name!: string;

  @Field(()=>String)
  @Column({ type: 'varchar', length: 50, nullable: true })
  state!: string;

  @Field(()=>User)
  @ManyToOne(()=> User, user=>user.addresses)
  @JoinColumn()
  user: User
}

@InputType()
export class InputAdrress{
  @Field(()=>String)
  name?: string;

  @Field(()=>String)
  state?: string;
}

@Exclude()
@InputType()
export class UpdateAdrress{
  @Expose()
  @Field(()=>String, {nullable:true})
  name!: string;

  @Expose()
  @Field(()=>String, {nullable:true})
  state!: string;
}