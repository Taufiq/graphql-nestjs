

export interface IJwtPayload {
  id: number;
  user_name: string;
}
