import { Field, InputType, ObjectType } from '@nestjs/graphql';
import {
  Entity,
  Column,
  Unique,
  OneToMany,
} from 'typeorm';
import { Skill } from './skill.entity';
import { Content } from './content.entity';



@Entity('users')
@ObjectType()
@Unique(['user_name'])
export class User extends Content {
  @Field(()=> String)
  @Column({ type: 'varchar', length: 50 })
  user_name!: string;

  @Field(()=> String)
  @Column({ type: 'varchar', length: 50 })
  name!: string;

  @Column({ type: 'varchar', length: 250 })
  password!: string;

  @Field(() => Boolean)
  @Column({ type: 'boolean', default:false })
  is_active!: boolean;


  @Field(()=> [Skill])
  @OneToMany(()=>Skill, skill => skill.user, { eager: true })
  addresses: Skill[]

}
@InputType()
export class InputUserType {
  @Field()
  user_name!:string;

  @Field(()=> String)
  @Column({ type: 'varchar', length: 50 })
  name!: string;

  @Field()
  password!:string;


}

@InputType()
export class UpdateUser{
  @Field(() => Boolean)
  is_active?: boolean;

  @Field()
  password!:string;

}